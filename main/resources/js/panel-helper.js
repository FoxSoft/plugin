AJS.toInit(function ($) {
    var PLUGIN_KEY = "com.atlassian.plugins.tutorial.confluence-highlight-actions-demo-plugin:show-matched-text";
    Confluence && Confluence.HighlightAction && Confluence.HighlightAction.registerButtonHandler(PLUGIN_KEY, {
        onClick: function (selectionObject) {
            Confluence.HighlightDemoDialogs.showHighlightDemoDialog(selectionObject);
            Confluence.HighlightDemoDialogs
        },
        shouldDisplay: Confluence.HighlightAction.WORKING_AREA.MAINCONTENT_ONLY
    });
});